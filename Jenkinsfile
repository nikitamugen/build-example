#!groovy

/*
The MIT License
Copyright (c) 2015-, CloudBees, Inc., and a number of other of contributors
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.
        THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

import java.util.concurrent.CancellationException;
import hudson.AbortException;
import hudson.model.*;

node {

	currentBuild.result = "SUCCESS"

  String DEFAULT_SOURCE_BRANCH = 'master'

	String jobLinkText = "<${env.JOB_URL}|Задача #${env.JOB_NAME}>"
	String buildLinkText = "<${env.JOB_URL}${env.BUILD_NUMBER}|Сборка #${env.BUILD_NUMBER}>"
	String reportLinkText = "<${env.JOB_URL}${env.BUILD_NUMBER}/consoleText|Открыть отчет>"

	Date started = new Date()
	String startedText = started.format("dd.MM.yyyy HH:mm:ss", TimeZone.getTimeZone('UTC'))
	String endedText = "-"

	String message = "${jobLinkText} | ${buildLinkText} | ${reportLinkText}"

  try {

    // Загрузить файл с общими методами
    //
    sh "curl --request GET " +
       "     --header 'PRIVATE-TOKEN: ${env.GITLAB_TOKEN}' " +
       "     --output 'Jenkinsfile.common' " +
       "https://gitlab.com/nikitamugen/build-example/raw/master/Jenkinsfile.common"

    def common = load 'Jenkinsfile.common'

    // Определить целевую ветку для сборки
    //
    if (env.gitlabSourceBranch?.trim())
        env.BRANCH_NAME = env.gitlabSourceBranch
    else
        env.BRANCH_NAME = DEFAULT_SOURCE_BRANCH

 		print "env.BRANCH_NAME: ${env.BRANCH_NAME}"
    print "env.gitlabSourceBranch: ${env.gitlabSourceBranch}"

    stage 'Checkout'

    scmParams = [
      $class: 'GitSCM',
      branches: [[name: "origin/${env.BRANCH_NAME}"]],
      doGenerateSubmoduleConfigurations: false,
      submoduleCfg: [],
      userRemoteConfigs: [[
        credentialsId: 'GitLab-MeeseeksBot',
        name: 'origin',
        url: "${env.gitlabSourceRepoHttpUrl}"
      ]]
    ]
    if (env.gitlabTargetBranch?.trim())
      scmParams.extensions = [[
        $class: 'PreBuildMerge',
        options: [
          fastForwardMode: 'FF',
          mergeRemote: 'origin',
          mergeStrategy: 'default',
          mergeTarget: "${env.gitlabTargetBranch}"
        ]
      ]]

    checkout changelog: true, poll: true, scm: scmParams

    stage 'Test'

  	gitlabCommitStatus("test") {
      env.NODE_ENV = "node772"
      print "Environment will be : ${env.NODE_ENV}"

      common.notify env.JOB_NAME, env.BUILD_NUMBER, 'Тестирование', message, 'good', startedText, endedText, 'jenkins'
      sleep 3
    }

    stage 'Build'

  	gitlabCommitStatus("build") {
      common.notify env.JOB_NAME, env.BUILD_NUMBER, 'Сборка', message, 'good', startedText, endedText, 'jenkins'
      sleep 3
    }

    stage 'Deploy'

  	gitlabCommitStatus("deploy") {
      common.notify env.JOB_NAME, env.BUILD_NUMBER, 'Развертывание', message, 'good', startedText, endedText, 'jenkins'

      try {
        build job: 'deploy-example', parameters: [
          new hudson.model.StringParameterValue('job', env.JOB_NAME),
          new hudson.model.StringParameterValue('build', env.BUILD_NUMBER)
        ]
      } catch (CancellationException x) {
        println 'CancellationException'
        currentBuild.description += " (Установка отменена)"
        currentBuild.description.trim()
      } catch (AbortException x) {
        println 'AbortException'
        currentBuild.description += " (Установка отменена)"
        currentBuild.description.trim()
      }
      //load "Jenkinsfile.deploy"
    }

  } catch (err) {

  	currentBuild.result = "FAILURE"

  	throw err
      
  } finally {

    def common = load "Jenkinsfile.common"

    Date ended = new Date()
    endedText = ended.format("dd.MM.yyyy HH:mm:ss", TimeZone.getTimeZone('UTC'))

    if (currentBuild.result == 'FAILURE') {

      common.notify(env.JOB_NAME, env.BUILD_NUMBER, 'Ошибка', message, 'danger', startedText, endedText, 'jenkins')

    } else {

      common.notify(env.JOB_NAME, env.BUILD_NUMBER, 'Завершена', message, 'good', startedText, endedText, 'jenkins')

    }
  }

}